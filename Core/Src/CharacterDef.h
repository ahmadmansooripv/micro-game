/*
 * CharacterDef.h
 *
 *  Created on: Jul 1, 2020
 *      Author: sub
 */

#ifndef SRC_CHARACTERDEF_H_
#define SRC_CHARACTERDEF_H_

unsigned char EnemyLVL1[] = {
  0b00000,
  0b00100,
  0b01010,
  0b01110,
  0b00100,
  0b01110,
  0b10101,
  0b10101
};

#endif /* SRC_CHARACTERDEF_H_ */
